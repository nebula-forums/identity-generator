# Nebula Identity Generator
This is a program that will generate an identity for use on the Nebula network. It uses the Argon2 aglorithm and requires an RSA public key and a username to generate an identity.

# Usage guide
1. Clone the repo (or just download IdentityGenerator)
2. Ensure you have the necessary packages installed on your system
3. run `chmod +x IdentityGenerator`
4. Generate a 2048 bit RSA keypair

To generate an identity use 
```
IdentityGenerator -p <Public Key from your RSA keypair> -u <Username> -z <Min leading zeros>
```
Addtionally, you may specify `-o <Output file>` and `-v` for verbose mode.


To find out the current required leading zero count, refer to the current spec.


# Prerequisite python packages
1. sys
2. getopt
3. argon2-cffi
4. hashlib
5. base64